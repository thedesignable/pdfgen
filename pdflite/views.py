from django.views.generic import View
from django.utils import timezone
from .models import *
from .render import Renderer
# Create your views here.


class Pdf(View):


    def get(self, request):
        certificates = Certificates.objects.all()
        today = timezone.now()
        params = {
            'today': today,
            'certificates': certificates,
            'request': request
        }

        return Renderer.render('pdf.html', params)