from django.apps import AppConfig


class PdfliteConfig(AppConfig):
    name = 'pdflite'
