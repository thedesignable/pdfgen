from django.db import models
from django.contrib.auth.models import User


class Collections(models.Model):
    title = models.CharField(max_length=255)
    event = models.DecimalField(default=0.00, max_digits=18, decimal_places=2)

    def __str__(self):
        return self.title

    class Meta:
        db_table = "tutorial_Collections"
        verbose_name = "Collection"
        verbose_name_plural = "Collections"


class Certificates(models.Model):
    collection = models.ForeignKey(Collections, on_delete=None)
    event_total = models.IntegerField(default=0)
    event = models.DecimalField(default=0.00, max_digits=18, decimal_places=2)
    customer = models.ForeignKey(User, on_delete=None)
    created_at = models.DateTimeField(auto_now_add=True)
    
# make sure to return the str explicity the collection or it wont work
    def __str__(self):
        return str(self.collection)

    def save(self, *args, **kwargs):
        self.event = self.collection.event * self.event_total
        super(Certificates, self).save(*args, **kwargs)

    class Meta:
        db_table = "tutorial_certificates"
        verbose_name = "Certificate"
        verbose_name_plural = "Certificates"
